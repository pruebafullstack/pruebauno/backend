//PENDIENTE DE SEGUIR INTENTANDO LA CREACIÓN DE LA PRUEBA
const {
  crearCita,
  obtenerCitas,
  actualizarCita,
  eliminarCita,
} = require("../controllers/controllerCitas");


const Cita = require('../models/cita');
const { validationResult } = require('express-validator');

jest.mock('../models/cita');
jest.mock('express-validator');

describe('Pruebas para la función crearCita', () => {
  test('debería crear una nueva cita', async () => {
    // Mock de req y res
    const req = {
      body: {
        nombre: 'Prueba1', 
        /* creado:'2022-11-16T23:00:37.564+00:00', */// Proporciona un nombre válido para la cita
      },
      usuario: {
        id: '636d16fbe2dd7a99520bb06d', // Proporciona un ID de usuario válido
      },
    };
    const res = {
      json: jest.fn(),
      status: jest.fn(),
    };

    // Mock de errores de validación
    validationResult.mockReturnValue({
      isEmpty: jest.fn().mockReturnValue(true),
    });

    // Mock de la función save de la clase Cita
    const saveMock = jest.fn().mockResolvedValue();

    // Mock de la creación de una nueva instancia de Cita
    Cita.mockReturnValueOnce({
      save: saveMock,
    });

    await crearCita(req, res);

    // Verificar que la función de validación de errores haya sido llamada
    expect(validationResult).toHaveBeenCalledWith(req);

    // Verificar que la función de creación de instancia de Cita haya sido llamada con los datos correctos
    expect(Cita).toHaveBeenCalledWith({
      nombre: req.body.nombre,
      creador: req.usuario.id,
    });

    // Verificar que la función save de la instancia de Cita haya sido llamada
    expect(saveMock).toHaveBeenCalled();

    // Verificar que se haya enviado una respuesta JSON con la cita creada
    expect(res.json).toHaveBeenCalledWith(expect.any(Object));

    // Verificar que no se haya llamado a la función de estado de respuesta con un código de error
    expect(res.status).not.toHaveBeenCalledWith(expect.any(Number));
  });
});


/*describe("Pruebas para las funciones de citas", () => {
   // Prueba para la función crearCita
  test("crearCita debería crear una nueva cita", async () => {
    // Mock de req y res
    const req = {
      body: {
        // Proporciona los datos necesarios para crear una cita
      },
      usuario: {
        id: "usuario-id", // Proporciona un ID de usuario válido
      },
    };
    const res = {
      json: jest.fn(),
    };

    await crearCita(req, res);

    // Verifica el comportamiento esperado
    expect(res.json).toHaveBeenCalled();
    // Agrega más expectativas según el comportamiento esperado
  }); */
/* 
  // Prueba para la función obtenerCitas
  test("obtenerCitas debería devolver las citas del usuario", async () => {
    // Mock de req y res
    const req = {
      usuario: {
        id: "usuario-id", // Proporciona un ID de usuario válido
      },
    };
    const res = {
      json: jest.fn(),
    };

    await obtenerCitas(req, res);

    // Verifica el comportamiento esperado
    expect(res.json).toHaveBeenCalled();
    // Agrega más expectativas según el comportamiento esperado
  });

  // Prueba para la función actualizarCita
  test("actualizarCita debería actualizar una cita existente", async () => {
    // Mock de req y res
    const req = {
      body: {
        // Proporciona los datos necesarios para actualizar la cita
      },
      params: {
        id: "cita-id", // Proporciona un ID de cita válido
      },
      usuario: {
        id: "usuario-id", // Proporciona un ID de usuario válido
      },
    };
    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };

    await actualizarCita(req, res);

    // Verifica el comportamiento esperado
    expect(res.json).toHaveBeenCalled();
    // Agrega más expectativas según el comportamiento esperado
  });

  // Prueba para la función eliminarCita
  test("eliminarCita debería eliminar una cita existente", async () => {
    // Mock de req y res
    const req = {
      params: {
        id: "cita-id", // Proporciona un ID de cita válido
      },
      usuario: {
        id: "usuario-id", // Proporciona un ID de usuario válido
      },
    };
    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };

    await eliminarCita(req, res);

    // Verifica el comportamiento esperado
    expect(res.json).toHaveBeenCalled();
    // Agrega más expectativas según el comportamiento esperado
  }); 
});
*/