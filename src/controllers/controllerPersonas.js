const Persona = require("../models/persona");
const { validationResult } = require("express-validator");

exports.crearPersona = async (req, res) => {
  //revisar si hay errores
  console.log("entro al BACK",req, res)
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(401).json({ errores: errores.array() });
  }

  try {
    //crear un nuevo Persona
    const persona = new Persona(req.body);

    persona.creador = req.usuario.id;

    persona.save();
    res.json(persona);
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};

exports.obtenerPersonas = async (req, res) => {
  try {
    const personas = await Persona.find({ creador: req.usuario.id }).sort({
      creado: -1,
    });
    res.json({ personas });
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};

exports.actualizarPersona = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(401).json({ errores: errores.array() });
  }

  const { documento,nombre, apellido } = req.body;
  const nuevoPersona = {};

  if (nombre) {
    nuevoPersona.documento = documento;
    nuevoPersona.nombre = nombre;
    nuevoPersona.apellido = apellido;
  }

  try {
    let persona = await Persona.findById(req.params.id);

    if (!persona) {
      return res.status(400).json({ msg: "Persona no encontrado" });
    }

    if (persona.creador.toString() !== req.usuario.id) {
      return res.status(400).json({ msg: "No autorizado" });
    }

    persona = await Persona.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: nuevoPersona },
      { new: true }
    );

    res.json({ persona });
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};

exports.eliminarPersona = async (req, res) => {
  try {

    console.log("LLEGA A REQ 2", req.params)

    let persona = await Persona.findById(req.params.id);  

    console.log("LLEGA A ELIMINAR 2", persona)

    if (!persona) {      
      return res.status(400).json({ msg: "Persona no encontrado" });
    }
    
    if (persona.creador.toString() !== req.usuario.id) {      
      return res.status(400).json({ msg: "No autorizado" });
    }        

    await Persona.remove({ _id: req.params.id });
    res.json({ msg: "Persona eliminada" });

  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};
