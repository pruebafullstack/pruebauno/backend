const Tratamiento = require("../models/tratamiento");
const { validationResult } = require("express-validator");

/* function saveTratamiento(req, res) {
    var newTratamiento = new Tratamiento(req.body);
    newTratamiento.save((err, result) => {
        res.status(200).send({message:result});
    });
} */

exports.crearTratamiento = async (req, res) => {
    console.log("entro al back",req, res)
    //revisar si hay errores
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
      return res.status(401).json({ errores: errores.array() });
    }
  
    try {
      //crear un nuevo Tratamiento
      const tratamiento = new Tratamiento(req.body);
        console.log("entra al try", tratamiento)
      tratamiento.creador = req.usuario.id;
  
      tratamiento.save();
      res.json(tratamiento);
    } catch (error) {

      console.log("Hubo un error");
      console.log(error);
      res.status(400).send("Hubo un error");
    }
  };

exports.obtenerTratamiento = async (req, res) => {
    try {
      const tratamiento = await Tratamiento.find({ creador: req.usuario.id }).sort({
        creado: -1,
      });
      res.json({ tratamiento });
    } catch (error) {
      console.log("Hubo un error");
      console.log(error);
      res.status(400).send("Hubo un error");
    }
  };

  exports.actualizarTratamiento = async (req, res) => {
    //revisar si hay errores
    const errores = validationResult(req);
    if (!errores.isEmpty()) {
      return res.status(401).json({ errores: errores.array() });
    }
  
    const { nombre } = req.body;
    const nuevoTratamiento = {};
  
    if (nombre) {
      nuevoTratamiento.nombre = nombre;
    }
  
    try {
      let tratamiento = await Tratamiento.findById(req.params.id);
  
      if (!tratamiento) {
        return res.status(400).json({ msg: "Tratamiento no encontrado" });
      }
  
      if (tratamiento.creador.toString() !== req.usuario.id) {
        return res.status(400).json({ msg: "No autorizado" });
      }
  
      tratamiento = await Tratamiento.findByIdAndUpdate(
        { _id: req.params.id },
        { $set: nuevoTratamiento },
        { new: true }
      );
  
      res.json({ tratamiento });
    } catch (error) {
      console.log("Hubo un error");
      console.log(error);
      res.status(400).send("Hubo un error");
    }
  };


exports.eliminarTratamiento = async (req, res) => {
    try {
      let tratamiento = await Tratamiento.findById(req.params.id);
  
      if (!tratamiento) {
        return res.status(400).json({ msg: "Tratamiento no encontrado" });
      }
  
      if (tratamiento.creador.toString() !== req.usuario.id) {
        return res.status(400).json({ msg: "No autorizado" });
      }
  
      await Tratamiento.remove({ _id: req.params.id });
      res.json({ msg: "Tratamiento eliminado" });
    } catch (error) {
      console.log("Hubo un error");
      console.log(error);
      res.status(400).send("Hubo un error");
    }
  };
  
/* 
function listarAllData(req, res) {
    var idTratamiento = req.params.id;
    if(!idTratamiento) {
        var result = Tratamiento.find({}).sort('nombre');
    } else {
        var result = Tratamiento.find({Tratamiento:idTratamiento}).sort('nombre');
    }
    result.exec(function(err, result) {
        if(err) {
            res.status(500).send({message: 'Error al momento de ejecutar la solicitud'});
        } else {
            if(!result) {
                res.status(404).send({message: 'El registro a buscar no se encuentra disponible'});
            } else {
                res.status(200).send({result});
            }
        }
    });
}

function updateTratamiento(req,res) {
    var id = req.params.id;
    Tratamiento.findByIdAndUpdate({_id:id}, req.body, {new:true},
        function(err,Tratamiento) {
        if(err){
            res.send(err);
        }
        res.json(Tratamiento);
    });
}

function deleteTratamiento(req, res) {
    var idTratamiento = req.params.id;
    Tratamiento.findByIdAndDelete(idTratamiento, function (err, tratamiento) {
        if (err) {
            return res.json(500, {
                message: 'No encontramos este tratamiento',
                error: err.toString()
            });
        }
        return res.json(tratamiento);
    });
}

function buscarDataId(req, res) {
    var idTratamiento = req.params.id;
    Tratamiento.findById(idTratamiento).exec((err, result) => {
        if(err) {
            res
            .status(500)
            .send({message: 'Error al momento de ejecutar la solicitud'});
        } else {
            if(!result) {
                res
                .status(404)
                .send({message: 'El registro a buscar no se encuentra disponible'});
            } else {
                res.status(200).send({result});
            }
        }
    });
} */

/* module.exports = {
    saveTratamiento,
    listarAllData,
    buscarDataId,
    updateTratamiento,
    deleteTratamiento
} */