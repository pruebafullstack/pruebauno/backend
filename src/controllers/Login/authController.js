const Usuario = require("../../models/Login/Usuario")
const bcryptjs = require("bcryptjs");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");

//PARA INGRESAR CON UN USUARIO REGISTRADO
exports.autenticarUsuario = async (req, res) => {

  console.log("usuario req",req)

  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(400).json({ errores: errores.array() });
  }

  const { email, password } = req.body;

  try {
    //revisar que sea un usuario registrado
    /* let x = new Promise (Usuario.findOne({ email }))
    await x;
    console.log("usuario registrado", x) */
    let usuario = await Usuario.findOne({ email });
    console.log("usuario registrado", usuario) 
    if (!usuario) { 
    /* if (!x) { */
      return res.status(400).json({ msg: "El usuario no existe" });
    }

    //revisar la password
    const passCorrecto = await bcryptjs.compare(password, usuario.password);
    if (!passCorrecto) {
      return res.status(400).json({ msg: "Contraseña incorrecta" });
    }

    //Si todo es correcto, crear y firmar el token

    const payload = {
      usuario: { id: usuario.id },
      //
      /* usuario: { nombre: usuario.nombre }, */
    };

    console.log("PAY LOAD JAIME", payload)

    jwt.sign(
      payload,
      process.env.SECRETA,
      {
        expiresIn: 43200, //1 hora
      },
      (error, token) => {
        if (error) throw error;

        //Mensaje de confirmación
        res.json({ token });
      }
    );
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};

exports.usuarioAutenticado = async (req, res) => {
  try {
    const usuario = await Usuario.findById(req.usuario.id);
    console.log("USUARIOUSUARIO", usuario)
    res.json({ usuario });
  } catch (error) {
    res.status(500).json({ msg: "Hubo un error" });
  }
};
