var mongoose = require("mongoose");

const PersonaSchema = mongoose.Schema({
  documento: {
    type: Number,
    required: true,
    trim: true,
  },
  nombre: {
    type: String,
    required: true,
    trim: true,
  },
  apellido: {
    type: String,
    required: true,
    trim: true,
  },
  creador: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Usuario",
  },
  creado: { type: Date, default: Date.now() },
});

module.exports = mongoose.model("Persona", PersonaSchema);
