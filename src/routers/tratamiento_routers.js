const controllerTratamiento = require("../controllers/controllerTratamiento");
const { Router } = require("express");
const router = Router();
const auth = require("../middleware/auth");
const { check } = require("express-validator");

router.post(
  "/",
  auth,
  [check("nombre", "El nombre del proyecto es obligatorio").not().isEmpty()],
  controllerTratamiento.crearTratamiento
);

router.get("/", auth, controllerTratamiento.obtenerTratamiento);

router.put(
  "/:id",
  auth,
  [check("nombre", "El nombre del proyecto es obligatorio").not().isEmpty()],
  controllerTratamiento.actualizarTratamiento
);

router.delete("/:id", auth, controllerTratamiento.eliminarTratamiento);

module.exports = router;

/* router.get('/buscarTratamiento/:id',tratamientoController.buscarDataId);
router.get('/buscarResultados/:idb?', tratamientoController.listarAllData);
router.post('/crearTratamiento', tratamientoController.saveTratamiento);
router.delete('/remove/:id',tratamientoController.deleteTratamiento);
router.put('/cambiar/:id',tratamientoController.updateTratamiento);

module.exports =  router; */