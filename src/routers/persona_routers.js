const { Router } = require("express");

const controllerPersona = require("../controllers/controllerPersonas");

const router = Router();

const auth = require("../middleware/auth");
const { check } = require("express-validator");

router.post(
  "/",
  auth,
  [check("nombre", "El nombre del proyecto es obligatorio").not().isEmpty()],
  controllerPersona.crearPersona
);

router.get("/", auth, controllerPersona.obtenerPersonas);

router.put(
  "/:id",
  auth,
  [check("nombre", "El nombre del proyecto es obligatorio").not().isEmpty()],
  controllerPersona.actualizarPersona
);

router.delete("/:id", auth, controllerPersona.eliminarPersona);

module.exports = router;
