const express = require("express");
const app = express();

// funciones especificas de express (v4.17), para utilizar JSON 
app.use(express.json());
app.use(express.urlencoded({  extended: true }));

/* configurar cabeceras y cors 
(elementos para restringir el acceso al backend (seguridad)
 si no existen los metodos dentro de estos cors arrojara error) */
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-with, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

//rutas
app.use('/',cita_routers);
app.use('/',tratamiento_routers);
app.use('/',persona_routers);

module.exports = app;